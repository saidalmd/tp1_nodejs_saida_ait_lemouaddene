const express=require("express");
const app=express();

app.use(express.json());

const equipes=require("./equipes.json");
const joueurs=require("./joueurs.json");

app.listen(82,()=>{
    console.log("node projet1 via express");
})
// cruds equipes
app.get("/equipes",(req,res)=>{
    res.status(200).json(equipes);
})
app.get("/equipes/:id",(req,res)=>{
    const id=parseInt(req.params.id);
    const equipe=equipes.find(equipe=>equipe.id===id);
    res.status(200).json(equipe);
})

app.post("/equipes",(req,res)=>{
    equipes.push(req.body)
    res.status(200).json(equipes)
})

app.put("/equipes/:id",(req,res)=>{
    const id=parseInt(req.params.id);
    let equipe=equipes.find(equipe=>equipe.id===id);
    equipe.name=req.body.name;
    equipe.country=req.body.country;
    res.status(200).json(equipe);
})

// 1-cruds joueurs
app.get("/joueurs",(req,res)=>{
    res.status(200).json(joueurs);
})
app.get("/joueurs/:id",(req,res)=>{
    const id=parseInt(req.params.id);
    const joueur=joueurs.find(joueur=>joueur.id===id);
    res.status(200).json(joueur);
})

app.post("/joueurs",(req,res)=>{
    joueurs.push(req.body)
    res.status(200).json(joueurs)
})

app.put("/joueurs/:id",(req,res)=>{
    const id=parseInt(req.params.id);
    let joueur=joueurs.find(joueur=>joueur.id===id);
    joueur.idEquipe=req.body.idEquipe;
    joueur.nom=req.body.nom;
    joueur.numero=req.body.numero;
    joueur.poste=req.body.poste;
    res.status(200).json(joueur);
})

app.delete("/joueurs/:id",(req,res)=>{
    const id=parseInt(req.params.id);
    let joueur=joueurs.find(joueur=>joueur.id===id);
    joueurs.splice(joueurs.indexOf(joueur),1);
    res.status(200).json(joueurs);
})

// 2-affciher les joueurs d'une équipe via son id
app.get("/equipes/:id/joueurs",(req,res)=>{
    const id=parseInt(req.params.id);
    let joeursEquipe=joueurs.filter(joueur=>joueur.idEquipe===id);
    res.status(200).json(joeursEquipe);

})

// 3-afficher l'équipe d'un joueur vias son id
app.get("/joueurs/:id/equipe",(req,res)=>{
    const id=parseInt(req.params.id);
    let joueur=joueurs.find(joueur=>joueur.id===id);
    let equipe=equipes.find(equipe=>equipe.id===joueur.idEquipe);
    res.status(200).json(equipe);
})

// 4-chercher un joueur à partir de son nom
app.get("/joueurs/nom/:nom",(req,res)=>{
    let joueur=joueurs.find(joueur=>joueur.nom===req.params.nom);
    res.status(200).json(joueur);
})